<!DOCTYPE html><?php include_once __DIR__.'/../src/assets.php' ?>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width" />
  <meta name="description" content="Temps restant avant la sortie de Kaamelott au cinéma">
  <meta name="keywords" content="Kaamelott, cinéma">

	<?php $title = 'Temps restant avant la sortie de Kaamelott au cinéma' ?>
	<title><?php echo $title ?></title>
	<link rel="stylesheet" href="<?php echo asset('build/app.css'); ?>">
</head>
<body>
	<main>
		<h1>Temps restant avant la sortie de <span class="kaa">Kaamelott</span> au cinéma</h1>

    <?php
    $today = new \DateTime();
    $dday = new \DateTime('2020-07-29');

    $remaining = $dday->diff($today)->days;
    ?>
		<p class="countdown"><?php echo $remaining ?> <span class="unit">jours</span></p>
	</main>
</body>
</html>
