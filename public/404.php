<!DOCTYPE html><?php include_once __DIR__.'/../src/assets.php' ?>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width" />

	<title>Cette page n'existe pas...</title>
	<link rel="stylesheet" href="<?php echo asset('build/app.css'); ?>">
</head>
<body>
<main>
	<h1>Cette page n'existe pas...</h1>
	<p class="label404">404</p>

	<figure class="image404">
		<img src="<?php echo asset('build/images/soupir.gif') ?>" alt="404">
	</figure>
</main>
</body>
</html>
