<?php

/**
 * @param string $asset
 *
 * @return string
 */
function asset(string $asset): string
{
    $manifest = @file_get_contents(__DIR__.'/../public/build/manifest.json');

    if ($manifest && ($manifest = @json_decode($manifest, true)) && isset($manifest[$asset])) {
        $asset = $manifest[$asset];
    }

    return $asset;
}
