<?php
/**
 * Config file used by Deployer to deploy this app in production
 */
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'kaamelottclock');

// Project repository
set('repository', 'gitlab.com:lobodol/kaamelottclock.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server
set('writable_dirs', []);

// Hosts
host('firediy.fr')
    ->user('root')
    ->set('deploy_path', '/var/www/{{application}}');

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

/**
 * Build assets locally using Webpack Encore
 */
task('build', function() {
    run('yarn encore prod');
})->local();

/**
 * Upload files to remote server.
 * (Only upload needed files)
 */
task('upload', function() {
    upload(__DIR__ . "/public", '{{release_path}}');
    upload(__DIR__ . "/src", '{{release_path}}');
});

task('release', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'upload',
    'deploy:shared',
    'deploy:writable',
    'deploy:symlink',
    'deploy:unlock',
]);

task('deploy', [
    'build',
    'release',
    'cleanup',
    'success'
]);
